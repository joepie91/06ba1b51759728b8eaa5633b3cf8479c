const basename = require('basename');
const Promise = require('bluebird');

const globAsync = Promise.promisify(require('glob'));

const dir = 'a';

function galleryListPromise() {
  return Promise.try( () => {
    return globAsync("gallery/*/", {})
  }).then((dirs) => {
    const realdirs = {};
    
    dirs.forEach((x) => {
      realdirs[basename(x)] = 1
    });
    
    return realdirs;
  });
}

function imageListPromise(galleryList) {
  return Promise.try(() => {
    if (galleryList[dir] === 1) {
      return globAsync(`gallery/${dir}/*jpg`, {});
    } else {
      return [];
    }
  }).then((imagelist) => {
    return [galleryList, imagelist];
  });
}

Promise.try(() => {
  return galleryListPromise();
}).then((galleryList) => {
  return imageListPromise(galleryList);
}).then((imageList) => {
  console.log(imageList);
});